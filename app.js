/**
 * SMS Parser Application file
 * Created by Roguyt
 */

/**
 * Load modules
 */

	const fs = require('fs');
	const async = require('async');
	const parseString = require('xml2js').parseString;
	const moment = require('moment');

/**
 * Parse xml file
 */

	fs.readFile('./sms.xml', (err, data) => {
		if (err) console.log(err);

		parseString(data, (err, data) => {
			if (err) console.log(err);

			let smses = "";
			async.each(data.smses.sms, (sms, callback) => {
				let date = moment.unix(sms.$.date / 1000).format('LLL');

				let message = "";

					message = "Numéro : " + sms.$.address + '\n' +
					"Date : " + date + '\n' +
					"Message: \n\n" + sms.$.body + '\n';


					smses = smses + message + '\n';
					callback();
			}, (err) => {
				if (err) console.log(err);

				async.each(data.smses.mms, (mms, callback) => {
					let date = moment.unix(mms.$.date / 1000).format('LLL');

					let message = "";

						message = "Numéro: " + mms.$.address + '\n' +
						"Date: " + date + '\n' +
						"Message: \n\n" + mms.parts[0].part[1].$.text

						smses = smses + message + '\n';
						callback();
				}, (err) => {
					if (err) console.log(err);

					fs.writeFile('./sms.txt', smses, (err) => {
						if (err) console.log(err);

						console.log("Done");
					})
				});
			});
		});
	});